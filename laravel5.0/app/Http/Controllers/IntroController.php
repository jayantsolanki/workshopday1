<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class IntroController extends Controller {

    /**
     * Show the name for the given user.
     *
     */

    public function intro(){
        return view('intro',['fname'=>'Jayant', 'lname'=>'Solanki', 'flag'=>'1']);
    }

}
