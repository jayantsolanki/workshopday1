<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
Route::get('intro', 'IntroController@intro'); //added intro controller, assignment 3
Route::get('insta', 'InstaController@Insta'); //added insta controller, assignment 4
Route::get('users', function()
{
    return 'Users!';
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/mypagejay/{Jayant}',function($Jayant)

{
	return "Hello {$Jayant}.";
});
