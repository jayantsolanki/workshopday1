<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
Bootstrap grid
</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class='container'>
	<div class='row'>
			<div class='col-md-12 custom_border-1'>
				<h3>Laravel controller</h3>
			
			@if($flag==1)
			<h3>{{'HELLO '.$fname." ".$lname}}{{', Displayed full name'}}</h3>
			@else
			{{$lname}}{{"displayed last name"}}
			@endif
			</div>
	</div>	
	
	<div class='row'>
		<div class='col-md-12 custom_border-1'>
			<h3>Example label with glyph icon 
				<span class="label label-primary">Home 
					<span class='glyphicon glyphicon-home'
					></span>
				</span>
			</h3>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. 
		
		</div>
	</div>
	<br/>
	<div class='row'>
		<div class='col-md-4 custom_border-2' style='color:brown;'>
		Column 
			<span class='badge'>1</span>
		 </br>
		Image will be displayed here 
			<span class='glyphicon glyphicon-picture'></span>
			</br>
			<a  target=blank href='http://instagram.com/jayantsolanki/'>
				<img id='im' style="display:none;" src='images/img1.jpg' class="img-responsive img-rounded" alt="Ponder over the far away buildings"/>
			</a>
		
		</div>
		<div class='col-md-4 custom_border-3' style='color:grey;'>
		Column 
		 <span class='badge'>
		 2
		 </span>
			<div class='alert alert-info'>
			You are seeing this alert box
			</div>
		 </br>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas.
		</div>
		<div class='panel panel-warning col-md-4 custom_border-4' style='color:gainsboro ;'>
		Column 
			 <span class='badge'>
			 3
			 </span>
			<div class='panel panel-heading'>
			Basic panel
			</div>
		 <div class="panel panel-body">
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas.
		</div>
		</div>
	</div>
	<br/>
	<div class='row'>
		<div class='well col-md-4 custom_border-5' style='color:cadetblue;'>
			<strong>
			Example of WELL 
			</strong>
		</br>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. 
		</div>
		<div class='col-md-4 col-md-offset-4 custom_border-6' style='color:grey;'>
			<a  href='javascript:alert("Alarm Alarm, run away!")'class="btn btn-danger glyphicon glyphicon-alert" type="submit"> Alert</a>
			<a href='javascript:showimg();' class="btn btn-primary glyphicon glyphicon-picture" type="submit"> image</a>
			<button class="btn btn-warning glyphicon glyphicon-modal-window" data-toggle="modal" data-target="#myModal" type="submit"> modal</button>
		</br>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas.
		</div>
		
	</div>
	<br/>
	<div class='row'>
		<div class='col-md-8 custom_border-6' style='color:cornflowerblue;'>
			<div class='col-md-4 custom_border-7' style='color:cornflowerblue;'>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
			Suspendisse sit amet justo ac metus aliquam bibendum.
			</div>
			<div class='col-md-4 custom_border-7' style='color:gray;'>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
			Suspendisse sit amet justo ac metus aliquam bibendum. 
			</div>
			<div class='col-md-4 custom_border-7' style='color:cornflowerblue;'>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
			Suspendisse sit amet justo ac metus aliquam bibendum. 
			</div>
		</div>
		<div class='col-md-4 custom_border-8' style='color:navy;'>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet.
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color:red;"aria-hidden="true">X</span></button>
				<h4 class="modal-title" id="myModalLabel">Arabian coast</h4>
			  </div>
			  <div class="modal-body">
				<a  target=blank href='http://instagram.com/jayantsolanki/'>
				<img id='im' src='images/img1.jpg' class="img-responsive img-rounded" alt="Ponder over the far away buildings"/>
				</a>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				
			  </div>
			</div>
		  </div>
		</div>
	</div>
	<div id="carousel-example-generic" class="col-md-6 col-md-offset-3 carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		<li data-target="#carousel-example-generic" data-slide-to="3"></li>
		<li data-target="#carousel-example-generic" data-slide-to="4"></li>
		<li data-target="#carousel-example-generic" data-slide-to="5"></li>
	  </ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">
		<div class="item active">
		  <img class='img-responsive img-rounded' src="images/img2.jpg" alt="...">
		  
		</div>
		<div class="item">
		  <img class='img-responsive img-rounded' src="images/img1.jpg" alt="...">
		  
		</div>
		<div class="item">
		  <img class='img-responsive img-rounded' src="images/img3.jpg" alt="...">
		
		</div>
		<div class="item">
		  <img class='img-responsive img-rounded' src="images/img4.jpg" alt="...">
		
		</div>
		<div class="item">
		  <img class='img-responsive img-rounded' src="images/img5.jpg" alt="...">
		
		</div>
		...
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
	</div>
</div>
<script type='text/javascript'>
function showimg()
{
    $('#im').show("slow");
} 
</script>
</body>
</html>
