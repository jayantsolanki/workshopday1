<!DOCTYPE html>
<html>
<head>
@include('includes.header')
</head>
<body>


<div class='container'>
<header class=r'row'>
	<!--@include('includes.nav')-->
</header>
	<div class='main'>
		@yield('content')

	</div><!--end of main-->
	<footer class='row'>
			<div class='col-md-12 panel '>
			@include('includes.footer')
	</footer>
	</div>
</div><!--end of container-->

</body>
</html>
