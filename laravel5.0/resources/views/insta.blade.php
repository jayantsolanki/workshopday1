@extends('layouts.barebone')
@section('content')
	<div class='row'>
		<div class='col-md-8 col-md-offset-2 panel'>
		<img class='img-responsive img-rounded' src='images/logo.png' alt='eyantra logo'>
		</div>
	</div>
	<div class='row'>
		<div class='col-md-6 '>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. 
		
		</div>
		<div class='col-md-6 '>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. Curabitur semper fermentum imperdiet. Phasellus ultricies urna vel ex scelerisque egestas. 
		
		</div>
	</div>
	<div class='row'>
		<div class='col-md-9 col-md-offset-2'>
		<h3 class='badge'>Images fetched from database</h3>
		</br>
		@foreach ($pics as $pic)
			<div class='col-md-5'>
       			<img class='img-responsive img-rounded' src="{{ $pic->src }}" alt="{{ $pic->name }}">
        	</div>
     	@endforeach
		</div>
	</div>
	
	<div class='row'>
		<div class='col-md-3 well'>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. 
		</div>
		<div class='col-md-3 col-md-offset-6 well'>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lacus sem. Nunc euismod vehicula tellus, id commodo felis tincidunt eget. 
		Suspendisse sit amet justo ac metus aliquam bibendum. Vivamus magna metus, semper in porttitor a, pellentesque vel dolor. 
		Sed vel elit libero. 
		</div>
	</div>
@stop
